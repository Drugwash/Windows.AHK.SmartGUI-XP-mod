## -= SmartGUI XP Creator by Rajat =-       
#### Modified by Drugwash for Win9x support

![](ss_smartguixpcreatormod_t.png)		

*(click **[here](ss_smartguixpcreatormod.png)** for the full size image)*

Last official version by **Rajat** was v**4.0**.		
For historical purposes I have made it available **[here](SmartGuiXP40_original_version.zip)**.

To compile succesfully a working copy of the script, you must first add the `bitmap` resources to **AutoHotkeySC.bin** in the AutoHotkey installation folder (make a copy of the original beforehand):

- **skin.bmp** as **100**
- **grid.bmp** as **101**
- **splash.bmp** as **102**

You may also replace icons in icon group **159** and **228** with **icon.ico**.     
All of the above can be found in the `\res` subfolder.

Known issues:

- When editing complex scripts, the result - when saved - may lack portions of code;        
please use a file comparison utility to detect and fix the problem, if present. BACKUP old script first!

#### What is SmartGUI XP Creator?

It is a complex WYSIWYG editor for **AutoHotkey 1.0** graphical interfaces (GUIs). AutoHotkey 1.0 - last known official version being **1.0.48.05** - also known as AutoHotkey **Basic** or **Classic**, is an ANSI-only version compatible with Windows® 9x (95/98/98SE/ME) that can also run on later Unicode Windows® like XP, Vista, 7 etc with the inherent limitations.

This editor has been very revered in its time, back when AHK 1.0 was still a thing. It is as obsolete now as the old ANSI-only AHK but for retro hobbyists and die-hard Windows® 9x fans it could still prove useful if they wanted to build their own small applications for the beloved systems.

The script/application should work fairly well under an enhanced 9x system. To be fair I developed and used this modified version under a heavily enhanced 98SE with KernelEx installed, and it worked OK nevermind the few existing bugs and shortcomings. I'm not sure how it would behave on a vanilla 9x or one insufficiently updated/enhanced. One may try it.

Running under XP<sup>TM</sup> or later is possible but it may suffer from specific issues and certain functions may not behave correctly. Frustrating but doable, depending on the complexity of the intended GUI.

#### How to use it?

There is a HTML formatted document in the package that acts as a basic manual for using this tool. It may be outdated in places where the mod changed behavior or added functions previously nonexistant.       
Also, download links will most likely not work anymore. Last known repository for this script is now at **[GitLab](https://gitlab.com/windows-wine/smartguixp-mod/)** and here at **[Codeberg](https://codeberg.org/Drugwash/Windows.AHK.SmartGUI-XP-mod/)**.

There has been a lot of work involved in creating and maintaining this script, both by Rajat and myself. Hope you find it useful. Enjoy!

**© Drugwash, 2009-2016, 2023**
